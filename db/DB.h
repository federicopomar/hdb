#ifndef _DB_H_
#define _DB_H_

#include <pthread.h>

typedef struct DBEntry {
	char* value;
	char* key;
	struct DBEntry* next;
	pthread_rwlock_t rwlock;
} DBEntry;

typedef struct DBInstance {
	pthread_rwlock_t rwlock;
	DBEntry * firstEntry;
} DBInstance;


DBInstance* dbCreateInstance();;
void dbSet(DBInstance* instance, const char* key, const char* value);
void dbUnset(DBInstance* instance, const char* key);
char* dbGet(DBInstance* instance, const char* key);
void dbFreeInstance(DBInstance* instance);
void dbResetInstance(DBInstance* instance);

#endif