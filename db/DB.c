#include "DB.h"

#include <stdlib.h>
#include <string.h>

#include <stdio.h>



DBEntry * dbRecursiveRemoveEntry(DBEntry*entry, const char* key);
DBEntry* dbRecursiveGetEntry(DBEntry*entry, const char*key);
DBInstance* dbCreateInstance();
DBEntry* dbCreateEntry(const char* key, const char* value);
void dbFreeEntry(DBEntry* entry);
void dbModifyEntryValue(DBEntry* entry, const char* value);
void dbRemoveEntry(DBInstance* instance, const char* key);
void dbAddEntry(DBInstance* instance, DBEntry* entry);
DBEntry* dbGetEntry(DBInstance* instance, const char* key);

DBEntry * dbRecursiveRemoveEntry(DBEntry*entry, const char* key) {
	DBEntry* nextEntry;
	if (entry && (!key || !strcmp(key, entry->key))) { // Found it
		nextEntry = entry->next;
		dbFreeEntry(entry);
		return nextEntry;
	} else if (entry) {
		entry->next = dbRecursiveRemoveEntry(entry->next, key);
		return entry;
	} else {
		return NULL;
	}
}

DBEntry* dbRecursiveGetEntry(DBEntry*entry, const char*key) {
	if (!entry || (key && (!strcmp(key, entry->key)))) {
		return entry;
	} else {
		return dbRecursiveGetEntry(entry->next, key);
	}
}


DBInstance* dbCreateInstance() {
	DBInstance* newInstance = malloc(sizeof(DBInstance));

	newInstance->firstEntry=NULL;

	if (pthread_rwlock_init(&(newInstance->rwlock), NULL)) {
		free(newInstance);
		newInstance = NULL;
	}

	return newInstance;
}

DBEntry* dbCreateEntry(const char* key, const char* value) {
	DBEntry* newEntry = malloc(sizeof(DBEntry));


	if (pthread_rwlock_init(&(newEntry->rwlock), NULL)) {
		free(newEntry);
		newEntry = NULL;
	}

	if (newEntry) {
		newEntry->value = malloc(strlen(value)+1);
		newEntry->key = malloc(strlen(key)+1);
		newEntry->next = NULL;
		strcpy(newEntry->value, value);
		strcpy(newEntry->key, key);
	}

	

	return newEntry;
}

void dbFreeEntry(DBEntry* entry) {
	pthread_rwlock_destroy(&(entry->rwlock));
	free(entry->key);
	free(entry->value);
	free(entry);
}

void dbModifyEntryValue(DBEntry* entry, const char* value) {
	pthread_rwlock_wrlock(&(entry->rwlock));
	free(entry->value);
	entry->value = malloc(strlen(value)+1);
	strcpy(entry->value, value);
	pthread_rwlock_unlock(&(entry->rwlock));
}


void dbRemoveEntry(DBInstance* instance, const char* key) {
	instance->firstEntry = dbRecursiveRemoveEntry(instance->firstEntry, key);
}

void dbAddEntry(DBInstance* instance, DBEntry* entry) {
	entry->next = instance->firstEntry;
	instance->firstEntry = entry;
}

DBEntry* dbGetEntry(DBInstance* instance, const char* key) {
	return dbRecursiveGetEntry(instance->firstEntry, key);
}

void dbSet(DBInstance* instance, const char* key, const char* value) {
	DBEntry * entry;

	pthread_rwlock_rdlock(&(instance->rwlock));

	entry = dbGetEntry(instance, key);
	if (entry) {
		dbModifyEntryValue(entry, value);
	} else {
		// Upgrade access to write lock to add entry
		pthread_rwlock_unlock(&(instance->rwlock));
		pthread_rwlock_wrlock(&(instance->rwlock));
		dbAddEntry(instance, dbCreateEntry(key, value));
	}

	pthread_rwlock_unlock(&(instance->rwlock));

}

void dbUnset(DBInstance* instance, const char* key) {
	pthread_rwlock_rdlock(&(instance->rwlock));
	if (dbGetEntry(instance, key)) {
		// Upgrade access to write lock to remove entry
		pthread_rwlock_unlock(&(instance->rwlock));
		pthread_rwlock_wrlock(&(instance->rwlock));
		dbRemoveEntry(instance, key);
	}
	pthread_rwlock_unlock(&(instance->rwlock));
}

char* dbGet(DBInstance* instance, const char* key) {
	DBEntry* entry;
	char* value = NULL;

	pthread_rwlock_wrlock(&(instance->rwlock));
	
	entry = dbGetEntry(instance, key);

	if (entry) {
		pthread_rwlock_rdlock(&(entry->rwlock));
		value = malloc(strlen(entry->value)+1);
		strcpy(value, entry->value);
		pthread_rwlock_unlock(&(entry->rwlock));
	}

	pthread_rwlock_unlock(&(instance->rwlock));

	return value;
}

void dbResetInstance(DBInstance* instance) {
	pthread_rwlock_wrlock(&(instance->rwlock));

	while(instance->firstEntry)
		dbRemoveEntry(instance, NULL);

	pthread_rwlock_unlock(&(instance->rwlock));
}

void dbFreeInstance(DBInstance* instance) {
	dbResetInstance(instance);
	pthread_rwlock_destroy(&(instance->rwlock));
	free(instance);
}

/*

void * threadFunction(void * data)
{
	DBInstance* instance = (DBInstance*) data;


	dbSet(instance, "randomKey", "randomValue");
	printf("randomKey value set\n");

	dbSet(instance, "anotherRandomKey", "anotherRandomValue");
	printf("anotherRandomKey value set\n");

	printf("Value of anotherRandomKey: %s\n", dbGet(instance, "anotherRandomKey"));
	printf("Value of randomKey: %s\n", dbGet(instance, "randomKey"));

	dbResetInstance(instance);

	
	return 0;
}


int main(int argc, char const *argv[])
{
	pthread_t thread[6];
	DBInstance* instance = dbCreateInstance();

	if (dbGet(instance, "randomKey"))
		printf("randomKey found! (What a Terrible Failure) \n");
	else
		printf("randomKey not found! (phew!)\n");

	pthread_create(&thread[0], NULL, threadFunction, instance);
	//pthread_detach(thread[0]);

	pthread_create(&thread[1], NULL, threadFunction, instance);
	//pthread_detach(thread[1]);

	pthread_create(&thread[2], NULL, threadFunction, instance);
	//pthread_detach(thread[2]);

	pthread_create(&thread[3], NULL, threadFunction, instance);
	//pthread_detach(thread[3]);

	pthread_create(&thread[4], NULL, threadFunction, instance);
	//pthread_detach(thread[4]);

	pthread_create(&thread[5], NULL, threadFunction, instance);
	//pthread_detach(thread[5]);
	
	pthread_join(thread[0], NULL);
	pthread_join(thread[1], NULL);
	pthread_join(thread[2], NULL);
	pthread_join(thread[3], NULL);
	pthread_join(thread[4], NULL);
	pthread_join(thread[5], NULL);

	if (dbGet(instance, "randomKey"))
		printf("randomKey found! (What a Terrible Failure) \n");
	else
		printf("randomKey not found! (phew!)\n");

	dbFreeInstance(instance);

	return 0;
}

*/