#include "srl/client/SRLClient.h"

#include <stdio.h>
#include <string.h>

#include "common.h"


SRLRequest* createGetRequest(const char* key) {
	GetRequest request;

	if (strlen(key) >= MAX_KEY_LENGTH)
		return NULL;

	strcpy(request.key, key);

	return srlCreateRequest(REQUEST_GET, sizeof(request), &request);
}

SRLRequest* createSetRequest(const char* key, const char* value) {
	SetRequest request;

	if (strlen(key) >= MAX_KEY_LENGTH || strlen(value) >= MAX_VALUE_LENGTH)
		return NULL;

	strcpy(request.key, key);
	strcpy(request.value, value);

	return srlCreateRequest(REQUEST_SET, sizeof(request), &request);

}

SRLRequest* createUnsetRequest(const char* key) {
	UnsetRequest request;

	if (strlen(key) >= MAX_KEY_LENGTH)
		return NULL;

	strcpy(request.key, key);

	return srlCreateRequest(REQUEST_UNSET, sizeof(request), &request);
}

void handleStatusResponse(StatusResponse* response) {
	if (response->error)
		printf("Error\n");
	else
		printf("OK\n");
}

void handleValueResponse(ValueResponse* response) {
	printf("%s\n", response->value);
}


void handleResponse(SRLResponse* response) {

	if (!response) {
		printf("Error requesting server\n");
		return;
	}

	switch ((ResponseType) response->type) {
		case RESPONSE_STATUS:
			handleStatusResponse((StatusResponse*) response->data);
			break;
		case RESPONSE_VALUE:
			handleValueResponse((ValueResponse*) response->data);
			break;
		default:
			printf("Unknown response type\n");
			break;
	}
}

void printUsage(const char* name) {

	if (!name)
		name = "client";

	printf("%s server (get/set/unset) key [value]\n", name);
	printf("Maximum Key Length: %d bytes\n", MAX_KEY_LENGTH);
	printf("Maximum Value Length: %d bytes\n", MAX_VALUE_LENGTH);

}

int main(int argc, char const *argv[])
{
	SRLRequest* request = NULL;
	SRLResponse* response = NULL;
	const char* server = argv[1];
	const char* action = argv[2];
	char const ** params = &(argv[3]);

	if (argc < 3) {
		printUsage(argv[0]);
		return 1;
	}

	if (!strcasecmp("get", action)) {
		request = createGetRequest(params[0]);
	} else if (!strcasecmp("set", action)) {

		if (argc < 4) {
			printUsage(argv[0]);
			return 1;
		}

		request = createSetRequest(params[0], params[1]);
	} else if (!strcasecmp("unset", action)) {
		request = createUnsetRequest(params[0]);
	}

	if (request) {
		srlInitialize("./srl/routes");
		response = srlSendRequest(server, request);
		handleResponse(response);
		srlFreeResponse(response);
		srlFreeRequest(request);
		srlTerminate();
	} else {
		printUsage(argv[0]);
		return 1;
	}

	return 0;
}