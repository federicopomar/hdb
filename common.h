#ifndef _COMMON_H_
#define _COMMON_H_

typedef enum {
	REQUEST_GET,
	REQUEST_SET,
	REQUEST_UNSET
} RequestType;

typedef enum
{
	RESPONSE_VALUE,
	RESPONSE_STATUS
} ResponseType;

#define MAX_KEY_LENGTH 32
#define MAX_VALUE_LENGTH 256

typedef struct 
{
	char key[MAX_KEY_LENGTH];
	char value[MAX_VALUE_LENGTH];
} SetRequest;

typedef struct 
{
	char key[MAX_KEY_LENGTH];
} GetRequest;

typedef struct 
{
	char key[MAX_KEY_LENGTH];
} UnsetRequest;

typedef struct
{
	char value[MAX_VALUE_LENGTH];
} ValueResponse;

typedef struct
{
	int error;
} StatusResponse;

#endif