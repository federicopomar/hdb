#ifndef _SRLCOMMONFIFO_H_
#define _SRLCOMMONFIFO_H_

#include "../SRLCommon.h"

typedef struct SRLFIFORequest {
	char* source;
	SRLRequest* request;
} SRLFIFORequest;

typedef struct SRLFIFOResponse
{
	char* dest;
	SRLResponse* response;
} SRLFIFOResponse;


SRLFIFOResponse* srlCreateFIFOResponse(char* dest, SRLResponse* response);
SRLFIFORequest* srlCreateFIFORequest(char* source, SRLRequest* request);
void srlFreeFIFORequest(SRLFIFORequest* fifoRequest);
void srlFreeFIFOResponse(SRLFIFOResponse* fifoResponse);

#endif