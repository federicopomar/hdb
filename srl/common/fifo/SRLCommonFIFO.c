
#include "SRLCommonFIFO.h"

#include <stdlib.h>
#include <string.h>

SRLFIFOResponse* srlCreateFIFOResponse(char* dest, SRLResponse* response) {
	SRLFIFOResponse* fifoResponse = (SRLFIFOResponse*) malloc(sizeof(SRLFIFOResponse));
	fifoResponse->dest = malloc(strlen(dest)+1);
	strcpy(fifoResponse->dest, dest);
	fifoResponse->response = response;
	return fifoResponse;
}

SRLFIFORequest* srlCreateFIFORequest(char* source, SRLRequest* request) {
	SRLFIFORequest* fifoRequest = (SRLFIFORequest*) malloc(sizeof(SRLFIFORequest));
	fifoRequest->source = malloc(strlen(source)+1);
	strcpy(fifoRequest->source, source);
	fifoRequest->request = request;
	return fifoRequest;
}

void srlFreeFIFORequest(SRLFIFORequest* fifoRequest) {
	if (fifoRequest) {
		free(fifoRequest->source);
		srlFreeRequest(fifoRequest->request);
	}
	free(fifoRequest);
}

void srlFreeFIFOResponse(SRLFIFOResponse* fifoResponse) {
	if (fifoResponse) {
		free(fifoResponse->dest);
		srlFreeResponse(fifoResponse->response);
	}
	free(fifoResponse);
}
