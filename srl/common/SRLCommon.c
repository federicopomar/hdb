#include "SRLCommon.h"
#include <stdlib.h>
#include <string.h>

SRLRequest* srlCreateRequest(msgtype_t type, msgsize_t length, void* data) {
	SRLRequest* newRequest = (SRLRequest*) malloc(sizeof(SRLRequest));
	newRequest->type = type;
	newRequest->length = length;
	newRequest->data = malloc(length);
	memcpy(newRequest->data, data, length);
	return newRequest;
}

SRLResponse* srlCreateResponse(msgtype_t type, msgsize_t length, void* data) {
	SRLResponse* newResponse = (SRLResponse*) malloc(sizeof(SRLResponse));
	newResponse->type = type;
	newResponse->length = length;
	newResponse->data = malloc(length);
	memcpy(newResponse->data, data, length);
	return newResponse;
}

void srlFreeRequest(SRLRequest* request) {
	if (request)
		free(request->data);
	free(request);
}

void srlFreeResponse(SRLResponse* response) {
	if (response)
		free(response->data);
	free(response);
}

