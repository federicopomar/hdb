#ifndef _SRLCOMMON_H_
#define _SRLCOMMON_H_

#define SRL_ERROR (-1)
#define SRL_OK (0)

typedef enum
{
	SRL_SOCKET,
	SRL_MESSAGEQUEUE,
	SRL_FIFO
} Protocol;

typedef unsigned short int msgsize_t;
typedef unsigned short int msgtype_t;

typedef struct SRLResponse {
	msgtype_t type;
	msgsize_t length;
	void* data;
} SRLResponse;

typedef struct SRLRequest {
	msgtype_t type;
	msgsize_t length;
	void* data;
} SRLRequest;

SRLRequest* srlCreateRequest(msgtype_t type, msgsize_t length, void* data);
SRLResponse* srlCreateResponse(msgtype_t type, msgsize_t length, void* data);
void srlFreeRequest(SRLRequest * request);
void srlFreeResponse(SRLResponse* response);

#endif