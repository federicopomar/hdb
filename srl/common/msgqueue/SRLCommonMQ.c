
#include "SRLCommonMQ.h"

#include <stdlib.h>
#include <string.h>

SRLMQResponse* srlCreateMQResponse(char* dest, SRLResponse* response) {
	SRLMQResponse* mQueueResponse = (SRLMQResponse*) malloc(sizeof(SRLMQResponse));
	mQueueResponse->dest = malloc(strlen(dest)+1);
	strcpy(mQueueResponse->dest, dest);
	mQueueResponse->response = response;
	return mQueueResponse;
}

SRLMQRequest* srlCreateMQRequest(char* source, SRLRequest* request) {
	SRLMQRequest* mQueueRequest = (SRLMQRequest*) malloc(sizeof(SRLMQRequest));
	mQueueRequest->source = malloc(strlen(source)+1);
	strcpy(mQueueRequest->source, source);
	mQueueRequest->request = request;
	return mQueueRequest;
}

void srlFreeMQRequest(SRLMQRequest* mQueueRequest) {
	if (mQueueRequest) {
		free(mQueueRequest->source);
		srlFreeRequest(mQueueRequest->request);
	}
	free(mQueueRequest);
}

void srlFreeMQResponse(SRLMQResponse* mQueueResponse) {
	if (mQueueResponse) {
		free(mQueueResponse->dest);
		srlFreeResponse(mQueueResponse->response);
	}
	free(mQueueResponse);
}