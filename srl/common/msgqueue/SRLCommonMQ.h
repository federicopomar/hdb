#ifndef _SRLCOMMONMQ_H_
#define _SRLCOMMONMQ_H_

#include "../SRLCommon.h"

#define MAX_MSG_SIZE 1024

typedef struct SRLMQRequest {
	char* source;
	SRLRequest* request;
} SRLMQRequest;

typedef struct SRLMQResponse
{
	char* dest;
	SRLResponse* response;
} SRLMQResponse;


SRLMQResponse* srlCreateMQResponse(char* dest, SRLResponse* response);
SRLMQRequest* srlCreateMQRequest(char* source, SRLRequest* request);
void srlFreeMQRequest(SRLMQRequest* mQueueRequest);
void srlFreeMQResponse(SRLMQResponse* mQueueResponse);

#endif