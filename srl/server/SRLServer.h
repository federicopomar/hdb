#ifndef _SRLSERVER_H_
#define _SRLSERVER_H_

#include "../common/SRLCommon.h"

typedef SRLResponse* (*SRLRequestCallback)(SRLRequest*);

int srlListen(Protocol protocol, SRLRequestCallback callback);



#endif