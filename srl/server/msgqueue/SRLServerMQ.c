#include "SRLServerMQ.h"

#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MQUEUE_PERMISSIONS 0666

SRLMQRequest* mQueueReadRequest(int mqdes) {
	//char tmpSource[1024];
	//char tmpData[1024];
	SRLMQRequest* request = NULL;
	char buffer[MAX_MSG_SIZE];
	msgsize_t * tmpSourceLengthPtr;
	char* tmpSource;
	msgtype_t* tmpTypePtr;
	msgsize_t* tmpLengthPtr;
	void* tmpData;

	mq_receive(mqdes, buffer, MAX_MSG_SIZE, 0);

	tmpSourceLengthPtr = (msgsize_t*) buffer;

	tmpSource = (char*) ((void*)tmpSourceLengthPtr+sizeof(msgsize_t));
	tmpTypePtr = (msgtype_t*) ((void*)tmpSource+(*tmpSourceLengthPtr));
	tmpLengthPtr = (msgsize_t*) ((void*)tmpTypePtr+sizeof(msgtype_t));
	tmpData = (void*) ((void*)tmpLengthPtr+sizeof(msgsize_t));

	request = srlCreateMQRequest(tmpSource, srlCreateRequest(*tmpTypePtr, *tmpLengthPtr, tmpData));

	return request;
}

int mQueueWriteResponse(SRLMQResponse* mQueueResponse) {
	int mqdes;
	
	if ((mqdes = mq_open(mQueueResponse->dest, O_WRONLY, MQUEUE_PERMISSIONS, NULL)) < 0)
		return SRL_ERROR;

	mq_send(mqdes, mQueueResponse->response->data, mQueueResponse->response->length, mQueueResponse->response->type);

	close(mqdes);

	return SRL_OK;

}

int mQueueHandleRequest(SRLMQRequest* mQueueRequest, SRLRequestCallback callback) {

	SRLMQResponse* mQueueResponse;

	if (mQueueRequest) {
		printf("Message From: %s\n", (char*)mQueueRequest->source);
		printf("Content: %s\n", (char*)mQueueRequest->request->data);
		mQueueResponse = srlCreateMQResponse(mQueueRequest->source, callback(mQueueRequest->request));
		mQueueWriteResponse(mQueueResponse);
		srlFreeMQResponse(mQueueResponse);
	}

	srlFreeMQRequest(mQueueRequest);

}

int srlMQueueListen(const char* filename, SRLRequestCallback callback) {
	mqd_t mqdes;
	struct mq_attr attr;
	SRLMQRequest* request;
	int serverListening = 1;

    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 1024;
    attr.mq_curmsgs = 0;

	mq_unlink(filename);

	// Lo abrimos RW para forzar que siempre haya un escritor
	// (nosotros mismos) de esta manera, bloquea siempre read.
	// Las escrituras de los clientes deben estar implementadas con
	// file locks o bien haciendo escrituras atomicas.\

	if ((mqdes = mq_open(filename, O_CREAT | O_RDONLY, MQUEUE_PERMISSIONS, &attr))<0){
		return SRL_ERROR; 
	}

	while (serverListening) {
		printf("Listening for request...\n");
		mQueueHandleRequest(mQueueReadRequest(mqdes), callback);
	}

	close(mqdes);
	mq_unlink(filename);
}