#ifndef _SRLSERVERMQUEUE_H_
#define _SRLSERVERMQUEUE_H_

#include "../SRLServer.h"
#include "../../common/msgqueue/SRLCommonMQ.h"

int srlMQueueListen(const char*filename, SRLRequestCallback callback);

#endif