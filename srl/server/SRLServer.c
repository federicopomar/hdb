
#include "SRLServer.h"

#include <assert.h>

int srlListen(Protocol protocol, SRLRequestCallback callback) {

	switch (protocol) {
		case SRL_SOCKET:
			return srlSocketListen(8080, callback);
		case SRL_MESSAGEQUEUE:
			return srlMQueueListen("/server", callback);
			break;
		case SRL_FIFO:
			return srlFIFOListen("/tmp/server", callback);
		default:
			assert(0);
			break;
	}

	return SRL_ERROR;

}
