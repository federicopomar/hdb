#include "SRLServerSocket.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <assert.h>
#include <pthread.h>

#define MAX_QUEUE 10

typedef struct SocketThreadData {
    int sockfd;
    SRLRequestCallback callback;
} SocketThreadData;


int socketWriteResponse(int sockfd, SRLResponse* response) 
{
    assert(sockfd != 0);
    
    if (response->length > 0)
    {
        if (send(sockfd, (void*) &(response->type), sizeof(response->type), 0) < 0)
            return SRL_ERROR;
        if (send(sockfd, (void*) &(response->length), sizeof(response->length), 0) < 0)
            return SRL_ERROR;
        if (send(sockfd, (void*) response->data, response->length, 0) < 0)
            return SRL_ERROR;
    }

    return SRL_OK;
}


SRLRequest* socketReadRequest(int sockfd)
{
    msgsize_t tmpLength;
    msgtype_t tmpType;
    void* tmpData;
    SRLRequest* request;
    assert(sockfd != 0);

    if (recv(sockfd, &tmpType, sizeof(tmpType), MSG_WAITALL) <= 0)
        return NULL;
    
    if (recv(sockfd, &tmpLength, sizeof(tmpLength), MSG_WAITALL) <= 0)
        return NULL;

    printf("len: %d\n", tmpLength);
    fflush(stdout);

    tmpData = malloc(tmpLength);
    if (recv(sockfd, tmpData, tmpLength, MSG_WAITALL) < tmpLength) {
        free(tmpData);
        return NULL;
    }

    request = srlCreateRequest(tmpType, tmpLength, tmpData);
    free(tmpData);

    return request;
}

void * socketThread(void* threadData) {
    SocketThreadData* data = (SocketThreadData*) threadData;
    socketHandleRequest(data->sockfd, data->callback);
    free(threadData);
    return NULL;
}

int socketHandleRequestThreaded(int sockfd, SRLRequestCallback callback) {
    pthread_t thread;
    SocketThreadData* data = malloc(sizeof(SocketThreadData));
    data->sockfd = sockfd;
    data->callback = callback;
    pthread_create(&thread, NULL, socketThread, data);
    pthread_detach(thread);
}

int socketHandleRequest(int sockfd, SRLRequestCallback callback) {

    SRLRequest* request;
    SRLResponse* response;

    request = socketReadRequest(sockfd);

    if (request == NULL)
        return SRL_ERROR;

    response = callback(request);

    if (response)
        socketWriteResponse(sockfd, response);

    close(sockfd);
    srlFreeResponse(response);
    srlFreeRequest(request);

    return SRL_OK;

}

int srlSocketListen(short int port, SRLRequestCallback callback) {
	struct sockaddr_in listenerAddress;
	int reuse = 1;
    int listener;
    int serverListening = 1;

	if ((listener = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
		return SRL_ERROR;

	if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
		return SRL_ERROR;

	memset(&listenerAddress, 0, sizeof(listenerAddress));

	listenerAddress.sin_family = AF_INET;
    listenerAddress.sin_addr.s_addr = INADDR_ANY;
    listenerAddress.sin_port = htons(port);

    if (bind(listener, (struct sockaddr *) &listenerAddress, sizeof(listenerAddress)) < 0)
    	return SRL_ERROR;

    if (listen(listener, MAX_QUEUE) < 0)
    	return SRL_ERROR;

    while (serverListening) {
    	socketHandleRequestThreaded(accept(listener, NULL, NULL), callback);
    }

    return SRL_OK;
}

