#ifndef _SRLSERVERSOCKET_H_
#define _SRLSERVERSOCKET_H_

#include "../SRLServer.h"

int srlSocketListen(short int port, SRLRequestCallback callback);

#endif