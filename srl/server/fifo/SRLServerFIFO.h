#ifndef _SRLSERVERFIFO_H_
#define _SRLSERVERFIFO_H_

#include "../SRLServer.h"
#include "../../common/fifo/SRLCommonFIFO.h"

int srlFIFOListen(const char*filename, SRLRequestCallback callback);

#endif