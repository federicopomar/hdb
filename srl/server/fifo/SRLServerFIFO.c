#include "SRLServerFIFO.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FIFO_PERMISSIONS 0666

SRLFIFORequest* fifoReadRequest(int fd) {
	msgsize_t tmpSourceLen;
	char* tmpSource = NULL;
	msgsize_t tmpLen;
	msgtype_t tmpType;
	void* tmpData = NULL;
	SRLFIFORequest* request = NULL;

	if (read(fd, &tmpSourceLen, sizeof(msgsize_t)) < sizeof(msgsize_t))
		return NULL;

	tmpSource = (char*) malloc(tmpSourceLen);

	if (read(fd, tmpSource, tmpSourceLen) < tmpSourceLen) {
		free(tmpSource);
		free(tmpData);
		return NULL;
	}

	if (read(fd, &tmpType, sizeof(msgtype_t)) < sizeof(msgtype_t)) {
		free(tmpSource);
		free(tmpData);
		return NULL;
	}

	if (read(fd, &tmpLen, sizeof(msgsize_t)) < sizeof(msgsize_t)) {
		free(tmpSource);
		free(tmpData);
		return NULL;
	}

	tmpData = malloc(tmpLen);

	if (read(fd, tmpData, tmpLen) < tmpLen) {
		free(tmpSource);
		free(tmpData);
		return NULL;
	}

	request = srlCreateFIFORequest(tmpSource, srlCreateRequest(tmpType, tmpLen, tmpData));

	free(tmpSource);
	free(tmpData);

	return request;
}

int fifoWriteResponse(SRLFIFOResponse* fifoResponse) {
	int fd;
	
	if ((fd = open(fifoResponse->dest, O_WRONLY)) < 0)
		return SRL_ERROR;

	write(fd, &(fifoResponse->response->type), sizeof(msgtype_t));
	write(fd, &(fifoResponse->response->length), sizeof(msgsize_t));
	write(fd, fifoResponse->response->data, fifoResponse->response->length);

	close(fd);

}

int fifoHandleRequest(SRLFIFORequest* fifoRequest, SRLRequestCallback callback) {

	SRLFIFOResponse* fifoResponse;

	if (fifoRequest) {
		printf("Message From: %s\n", (char*)fifoRequest->source);
		printf("Content: %s\n", (char*)fifoRequest->request->data);
		fifoResponse = srlCreateFIFOResponse(fifoRequest->source, callback(fifoRequest->request));
		fifoWriteResponse(fifoResponse);
		srlFreeFIFOResponse(fifoResponse);
	}

	srlFreeFIFORequest(fifoRequest);

}

int srlFIFOListen(const char*filename, SRLRequestCallback callback) {
	int fd;
	int serverListening = 1;
	SRLFIFORequest* request;

	unlink(filename);
	mkfifo(filename, FIFO_PERMISSIONS);

	// Lo abrimos RW para forzar que siempre haya un escritor
	// (nosotros mismos) de esta manera, bloquea siempre read.
	// Las escrituras de los clientes deben estar implementadas con
	// file locks o bien haciendo escrituras atomicas.
	if ((fd = open(filename, O_RDWR))<0)
			return SRL_ERROR;

	while (serverListening) {
		printf("Listening for request...\n");
		fifoHandleRequest(fifoReadRequest(fd), callback);
	}

	close(fd);
	unlink(filename);
}