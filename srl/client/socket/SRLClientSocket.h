#ifndef _SRLCLIENTSOCKET_H_
#define _SRLCLIENTSOCKET_H_


#include "../SRLClient.h"

SRLResponse* srlSocketSendRequest(char*hostName, int port, SRLRequest* request);

#endif