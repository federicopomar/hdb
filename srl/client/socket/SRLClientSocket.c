#include "SRLClientSocket.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <assert.h>

int socketWriteRequest(int sockfd, SRLRequest* request) 
{
	assert(sockfd != 0);
	
	if (request->length > 0)
	{
		if (send(sockfd, (void*) &(request->type), sizeof(request->type), 0) < 0)
			return SRL_ERROR;
		if (send(sockfd, (void*) &(request->length), sizeof(request->length), 0) < 0)
			return SRL_ERROR;
		if (send(sockfd, (void*) request->data, request->length, 0) < 0)
			return SRL_ERROR;
	}

	return SRL_OK;
}


SRLResponse* socketReadResponse(int sockfd)
{
	msgsize_t tmpLength;
	msgtype_t tmpType;
	void* tmpData;
	SRLResponse* response;
	assert(sockfd != 0);

	if (recv(sockfd, &tmpType, sizeof(tmpType), MSG_WAITALL) <= 0)
		return NULL;
	
	if (recv(sockfd, &tmpLength, sizeof(tmpLength), MSG_WAITALL) <= 0)
		return NULL;

	tmpData = malloc(tmpLength);
	if (recv(sockfd, tmpData, tmpLength, MSG_WAITALL) < tmpLength) {
		printf("%d\n",tmpLength);
		free(tmpData);
		return NULL;
	}

	response = srlCreateResponse(tmpType, tmpLength, tmpData);
	free(tmpData);

	return response;
}


SRLResponse* srlSocketSendRequest(char*hostName, int port, SRLRequest* request) {
	int sockfd;
	struct hostent * host;
	struct sockaddr_in hostAddress;
	SRLResponse* response;

	printf("Connect to %s:%d\n", hostName, port);

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return NULL;
	
	if ((host = gethostbyname(hostName)) == NULL)
		return NULL;

	memset(&hostAddress, 0, sizeof(hostAddress));
	hostAddress.sin_family = AF_INET;
	memcpy((void*)&(hostAddress.sin_addr.s_addr), (void *) host->h_addr, host->h_length);
	hostAddress.sin_port = htons(port);

	if (connect(sockfd, (struct sockaddr *) &hostAddress, sizeof(hostAddress)) < 0) {
		close(sockfd);
		return NULL;
	}

	if (socketWriteRequest(sockfd, request) == SRL_ERROR) {
		close(sockfd);
		return NULL;
	}

	if ((response = socketReadResponse(sockfd)) == NULL) {
		close(sockfd);
		return NULL;
	}

	return response;


}