
#include "SRLClientFIFO.h"

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define FIFO_PERMISSIONS 0666

SRLResponse* fifoReadResponse(char* fileName) {
	msgsize_t tmpLen;
	msgtype_t tmpType;
	void* tmpData = NULL;
	SRLResponse* response = NULL;
	int fd;

	if ((fd = open(fileName, O_RDONLY)) < 0) {
		free(tmpData);
		return NULL;
	}


	if (read(fd, &tmpType, sizeof(msgtype_t)) < sizeof(msgtype_t)) {
		free(tmpData);
		return NULL;
	}

	if (read(fd, &tmpLen, sizeof(msgsize_t)) < sizeof(msgsize_t)) {
		free(tmpData);
		return NULL;
	}

	tmpData = malloc(tmpLen);

	if (read(fd, tmpData, tmpLen) < tmpLen) {
		free(tmpData);
		return NULL;
	}

	response = srlCreateResponse(tmpType, tmpLen, tmpData);

	free(tmpData);

	return response;
}

int fifoWriteRequest(char* dest, SRLFIFORequest* fifoRequest) {
	int fd;
	struct flock lock;

	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	lock.l_pid = 0;

	msgsize_t sourceLen = sizeof(char)*(strlen(fifoRequest->source)+1);

	if ((fd = open(dest, O_WRONLY)) < 0)
		return SRL_ERROR;

	printf("Waiting for lock...\n");
	while(fcntl(fd, F_SETLKW, &lock) == -1);
	printf("Lock acquired\n");

	write(fd, &sourceLen, sizeof(msgsize_t));
	write(fd, fifoRequest->source, sourceLen);
	write(fd, &(fifoRequest->request->type), sizeof(msgtype_t));
	write(fd, &(fifoRequest->request->length), sizeof(msgsize_t));
	write(fd, fifoRequest->request->data, fifoRequest->request->length);

	lock.l_type = F_UNLCK;
	fcntl(fd, F_SETLKW, lock);

	printf("Lock released\n");

	close(fd);

	return SRL_OK;

}

SRLResponse* srlFIFOSendRequest(char* destFileName, char* sourceFileName, SRLRequest* request) {

	SRLFIFORequest* fifoRequest = NULL;

	unlink(sourceFileName);
	mkfifo(sourceFileName, FIFO_PERMISSIONS);

	fifoRequest = srlCreateFIFORequest(sourceFileName, srlCreateRequest(request->type, request->length, request->data));

	fifoWriteRequest(destFileName, fifoRequest);

	srlFreeFIFORequest(fifoRequest);

	return fifoReadResponse(sourceFileName);
}