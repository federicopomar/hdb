#ifndef _SRLCLIENTFIFO_H_
#define _SRLCLIENTFIFO_H_

#include "../SRLClient.h"
#include "../../common/fifo/SRLCommonFIFO.h"

SRLResponse* srlFIFOSendRequest(char* destFileName, char* sourceFileName, SRLRequest* request);

#endif