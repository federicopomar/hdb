#ifndef _SRLCLIENTMQUEUE_H_
#define _SRLCLIENTMQUEUE_H_

#include "../SRLClient.h"
#include "../../common/msgqueue/SRLCommonMQ.h"

SRLResponse* srlMQueueSendRequest(char* destFileName, char* sourceFileName, SRLRequest* request);

#endif