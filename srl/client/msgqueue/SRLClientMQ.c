
#include "SRLClientMQ.h"

#include <mqueue.h>
#include <unistd.h>
#include <fcntl.h>
#include <mqueue.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MQ_PERMISSIONS 0666

char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

SRLResponse* mQueueReadResponse(mqd_t mqdes) {
	msgsize_t tmpLen;
	unsigned tmpType;
	void* tmpData = NULL;
	SRLResponse* response = NULL;
	struct mq_attr attr;
	attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 1024;
    attr.mq_curmsgs = 0;

	tmpData = malloc(1024);

	tmpLen = mq_receive(mqdes, tmpData, MAX_MSG_SIZE, &tmpType);
	response = srlCreateResponse(tmpType, tmpLen, tmpData);

	free(tmpData);


	return response;

}

int mQueueWriteRequest(char* dest, SRLMQRequest* mQueueRequest) {
	mqd_t mqdes;
	char* msg;
	char* msgPtr;
	char* tmp;
	msgsize_t sourceLen = sizeof(char)*(strlen(mQueueRequest->source)+1);
	int msgSize = sourceLen+mQueueRequest->request->length+2*sizeof(msgsize_t)+sizeof(msgtype_t);

	if (msgSize > MAX_MSG_SIZE)
		return SRL_ERROR;

	if ((mqdes = mq_open(dest, O_WRONLY, MQ_PERMISSIONS, NULL)) < 0)
		return SRL_ERROR;

	msg = malloc(msgSize);
	msgPtr = msg;

	memcpy(msgPtr, &sourceLen, sizeof(msgsize_t));
	msgPtr += sizeof(msgsize_t);
	memcpy(msgPtr, mQueueRequest->source, sourceLen);
	msgPtr += sourceLen;
	memcpy(msgPtr, &(mQueueRequest->request->type), sizeof(msgtype_t));
	msgPtr += sizeof(msgtype_t);
	memcpy(msgPtr, &(mQueueRequest->request->length), sizeof(msgsize_t));
	msgPtr += sizeof(msgsize_t);
	memcpy(msgPtr, mQueueRequest->request->data, mQueueRequest->request->length);

	mq_send(mqdes, msg, msgSize, 0);

	free(msg);

	close(mqdes);

	return SRL_OK;

}

SRLResponse* srlMQueueSendRequest(char* destFileName, char* sourceFileName, SRLRequest* request) {

	SRLMQRequest* mQueueRequest = NULL;
	struct mq_attr attr;
	mqd_t mqdes;
	attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 1024;
    attr.mq_curmsgs = 0;

	mq_unlink(sourceFileName);

 	if ((mqdes = mq_open(sourceFileName, O_RDONLY | O_CREAT, MQ_PERMISSIONS, &attr))<0)
 		return NULL;

	mQueueRequest = srlCreateMQRequest(sourceFileName, srlCreateRequest(request->type, request->length, request->data));

	mQueueWriteRequest(destFileName, mQueueRequest);

	srlFreeMQRequest(mQueueRequest);

	return mQueueReadResponse(mqdes);
}
