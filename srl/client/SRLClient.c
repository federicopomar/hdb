#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "SRLClient.h"
#include "socket/SRLClientSocket.h"
#include "msgqueue/SRLClientMQ.h"
#include "fifo/SRLClientFIFO.h"

#include <time.h>

#define OCHO		8
#define BLOCK_SIZE	32
#define NBLOCK		OCHO

typedef void* RouteAddress;

typedef struct
{
	char* host;
	int port;
} SocketAddress;

typedef struct
{
	char* queue;
} MQAddress;

typedef struct 
{
	char* fileName;
} FIFOAddress;

typedef struct 
{
	char* name;
	Protocol proto;
	RouteAddress* addr;
	/* data */
} Route;

Route* routes = NULL;
int routeCount = 0;

int randomSeed = 0;

#define CLIENT_FIFO_FILE "/tmp/client%u-%u"
#define CLIENT_FILE_MAXLEN 256
#define CLIENT_MQUE_FILE "/client%u-%u"

int scanLine(char* line, char* name, char* protocol, char* address) {
	enum{NAME, PROTOCOL, ADDRESS, PORT};
	int state=NAME, port=-1;
	
	while(*line==' ' || *line=='\t')
		line++;
	
	while(*line != '\n') {
		if(*line==' ' || *line=='\t') {
			state++;
			while(*line==' ' || *line=='\t')
				line++;
			if(*line=='\n')
				return port;
		}
		
		switch(state) {
			case NAME:
			{
				*name=*line;
				name++;
				break;
			}
			case PROTOCOL:
			{
				*protocol=*line;
				protocol++;
				break;
			}
			case ADDRESS:
			{
				*address=*line;
				address++;
				break;
			}
			case PORT:
			{
				port =atoi(line);
				state++;
				break;
			}
			default:
				break;
		}
		line++;
	}
	return port;
}

int loadRoutingTable(char* filename) {

	FILE* filedes = fopen(filename, "r");
	
	if(filedes == NULL)
		return -1;

	int num, port;
	char line[BLOCK_SIZE*NBLOCK], *name, protocol[BLOCK_SIZE], *address;

	fgets(line, BLOCK_SIZE*NBLOCK, filedes);
	num = atoi(line);

	for( ; routeCount<num ;routeCount++) {
		name = (char *) malloc(BLOCK_SIZE);
		memset(protocol, 0, BLOCK_SIZE);
		address = (char *) malloc(BLOCK_SIZE);

		fgets(line, BLOCK_SIZE*NBLOCK, filedes);

		port = scanLine(line, name, protocol, address);
		
		if(!strcmp(protocol, "socket")) {
			routes = (Route*) realloc(routes, sizeof(Route)*(routeCount+1));
			routes[routeCount].name = (char*)name;
			routes[routeCount].proto = SRL_SOCKET;
			routes[routeCount].addr = (RouteAddress*) malloc(sizeof(SocketAddress));
			((SocketAddress*)routes[routeCount].addr)->host = (char*)address;
			((SocketAddress*)routes[routeCount].addr)->port = port;
			
		} else if(!strcmp(protocol, "mqueue")) {
			routes= (Route*) realloc(routes, sizeof(Route)*(routeCount+1));
			routes[routeCount].name = (char*)name;
			routes[routeCount].proto = SRL_MESSAGEQUEUE;
			routes[routeCount].addr = (RouteAddress*) malloc(sizeof(MQAddress));
			((MQAddress*)routes[routeCount].addr)->queue = (char*)address;
		
		} else if(!strcmp(protocol, "fifo")) {
			routes= (Route*) realloc(routes, sizeof(Route)*(routeCount+1));
			routes[routeCount].name = (char*)name;
			routes[routeCount].proto = SRL_FIFO;
			routes[routeCount].addr = (RouteAddress*) malloc(sizeof(FIFOAddress));
			((FIFOAddress*)routes[routeCount].addr)->fileName = (char*)address;
		}
	}

	fclose(filedes);
	return 0;
}


Route* getRoute(const char* name) 
{	
	int i;
	for (i = 0; i < routeCount; ++i)
	{
		if (!strcmp(name, routes[i].name))
			return routes+i;
	}

	return NULL;
}

SRLResponse* srlSendRequest(const char*dest, SRLRequest* request) {
	SocketAddress* sockAddr;
	MQAddress* mqueueAddr;
	FIFOAddress* fifoAddr;
	Route* route;
	char clientFile[CLIENT_FILE_MAXLEN];

	if (!randomSeed) {
		randomSeed = time(NULL);
		srand(randomSeed);
	}

	if (!(route = getRoute(dest))) 
		return NULL;

	switch (route->proto) {
		case SRL_SOCKET:
			sockAddr = (SocketAddress*) route->addr;
			return srlSocketSendRequest(sockAddr->host, sockAddr->port, request);
		case SRL_MESSAGEQUEUE:
			mqueueAddr = (MQAddress*) route->addr;
			sprintf(clientFile, CLIENT_MQUE_FILE, (unsigned int) getpid(), (unsigned int) rand());
			return srlMQueueSendRequest(mqueueAddr->queue, clientFile, request); 
		case SRL_FIFO:
			fifoAddr = (FIFOAddress*) route->addr;
			sprintf(clientFile, CLIENT_FIFO_FILE, (unsigned int) getpid(), (unsigned int) rand());
			return srlFIFOSendRequest(fifoAddr->fileName, clientFile, request);
		default:
			assert(0);
			break;
	}
}

int srlInitialize(char* rtfilename) {
	if (loadRoutingTable(rtfilename))
		return SRL_ERROR;
	return SRL_OK;
}

void srlTerminate() {
}
