#ifndef _SRLCLIENT_H_
#define _SRLCLIENT_H_

#include "../common/SRLCommon.h"

int srlInitialize(char* rtfilename);
void srlTerminate();

SRLResponse* srlSendRequest(const char*dest, SRLRequest* request);

#endif