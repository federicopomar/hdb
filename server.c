#include "srl/server/SRLServer.h"
#include "db/DB.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common.h"

DBInstance* dbInstance = NULL;

SRLResponse* createErrorResponse() {
	StatusResponse response;
	response.error = 1;
	return srlCreateResponse(RESPONSE_STATUS, sizeof(response), &response);
}

SRLResponse* createSuccessResponse() {
	StatusResponse response;
	response.error = 0;
	return srlCreateResponse(RESPONSE_STATUS, sizeof(response), &response);
}

SRLResponse* createGetResponse(const char* value) {
	ValueResponse response;
	strcpy(response.value, value);
	return srlCreateResponse(RESPONSE_VALUE, sizeof(response), &response);
}

SRLResponse* handleGetRequest(GetRequest* request) {
	SRLResponse* response = NULL;

	char* value = dbGet(dbInstance, request->key);

	if (value) {
		response = createGetResponse(value);
		free(value);
	}

	return response;
}


SRLResponse* handleSetRequest(SetRequest* request) {
	dbSet(dbInstance, request->key, request->value);
	return createSuccessResponse();
}

SRLResponse* handleUnsetRequest(UnsetRequest* request) {
	dbUnset(dbInstance, request->key);
	return createSuccessResponse();
}


SRLResponse* defaultResponseHandler(SRLRequest* request) {
	SRLResponse* response = NULL;

	switch ((RequestType) request->type) {
		case REQUEST_GET:
			response = handleGetRequest((GetRequest*) request->data);
			break;
		case REQUEST_UNSET:
			break;
		case REQUEST_SET:
			response = handleSetRequest((SetRequest*) request->data);
			break;
		default:
			break;
	}

	if (!response)
		response = createErrorResponse();

	return response;
}

int main(int argc, char const *argv[])
{
	dbInstance = dbCreateInstance();
	srlListen(SRL_MESSAGEQUEUE, defaultResponseHandler);
	dbFreeInstance(dbInstance);
	return 0;
}